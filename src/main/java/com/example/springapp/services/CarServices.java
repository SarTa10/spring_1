package com.example.springapp.services;

import com.example.springapp.jpa.Cars;
import com.example.springapp.jpa.specifications.CarsSpecification;
import com.example.springapp.repositories.CarsRepository;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.swing.text.html.HTMLDocument;
import java.util.List;

@Service
public class CarServices {

    private final CarsRepository carsRepository;

    public CarServices(CarsRepository carsRepository) {
        this.carsRepository = carsRepository;
    }

    public Object addCar(Cars car) {
        if (car == null || car.getCarBrand() !=null || car.getCarNumber() !=null || car.getCarType() != null|| car.getYear() != null) {
            return new Exception("No_args");
        }

        try {
            return this.carsRepository.save(car);
        } catch(Exception e) {
            return e;
        }
    }

    public List<Cars> searchCar(Cars car) {

        Specification<Cars> specification = Specification
                .where(CarsSpecification.idNotNull());

        if(car.getId() != null) {
            specification = specification.and(CarsSpecification.idEquals(car.getId()));
        }

        return this.carsRepository.findAll(specification);
    }

    public List<Cars> searchCarByBrand(Cars car) {
        Specification<Cars> specification = Specification
                .where(CarsSpecification.idNotNull());

        if(car.getCarBrand() != null) {
            specification = specification.and(CarsSpecification.getByCarBrand(car.getCarBrand()));
        }

        return this.carsRepository.findAll(specification);
    }

    public List<Cars> searchByYear(Cars car) {
        Specification <Cars> specification = Specification
                .where(CarsSpecification.idNotNull());
        if (car.getYear() != null) {
            specification = specification.and(CarsSpecification.getAllYears());
        }

        return this.carsRepository.findAll(specification);
    }

}
