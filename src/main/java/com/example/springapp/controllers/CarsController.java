package com.example.springapp.controllers;

import com.example.springapp.jpa.Cars;
import com.example.springapp.services.CarServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CarsController {
    private final CarServices carsService;

    @Autowired
    public CarsController(CarServices carsService) {
        this.carsService = carsService;
    }

    @PostMapping("cars/add")
    public Object addCar(@RequestBody Cars car){
        return carsService.addCar(car);
    }

    @PostMapping("/cars/search")
    public List<Cars> searchCars(@RequestBody Cars car) {
        return carsService.searchCar(car);
    }

}
