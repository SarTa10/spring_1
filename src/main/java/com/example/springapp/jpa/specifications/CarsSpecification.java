package com.example.springapp.jpa.specifications;

import com.example.springapp.jpa.Cars;
import org.springframework.data.jpa.domain.Specification;

public final class CarsSpecification {

    private  CarsSpecification() {

    }

    public static Specification<Cars> idEquals(Long id) {
        return (root, query, builder) -> builder.equal(root.get("id"),id);
    }

    public static Specification<Cars> idNotNull() {
        return (root, query, builder) -> builder.isNotNull(root.get("id"));
    }

    public static Specification<Cars> getAllYears() {
        return (root, query, builder) -> builder.isNotNull(root.get("year"));
    }

    public static Specification<Cars> getByCarBrand(String carBrand) {
        return (root, query, builder) -> builder.equal(root.get("carBrand"), carBrand);
    }
}
