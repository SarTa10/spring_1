package com.example.springapp.jpa;

public enum RecordState {
    DRAFT, ACTIVE, INACTIVE, DELETED
}
