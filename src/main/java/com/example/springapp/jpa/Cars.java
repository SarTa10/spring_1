package com.example.springapp.jpa;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name="CARS")
public class Cars extends  baseEntity<Long> {
    @Id
    @Column(name="ID")
    @SequenceGenerator(name="carsIdSeq", sequenceName = "cars_id_seq", allocationSize =1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "carsIdSeq")
    private Long id;

    @Column(name="CAR_NUMBER", nullable = false)
    private String carNumber;

    @Column(name="CAR_BRAND", nullable = false)
    private  String carBrand;

    @Column(name="CAR_TYPE", nullable = false)
    private String carType;

    @Column(name="YEAR", nullable = false)
    private Long year;
}
