package com.example.springapp.repositories;

import com.example.springapp.jpa.Cars;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface CarsRepository extends JpaRepository<Cars, Long>, JpaSpecificationExecutor<Cars> {

    List<Cars> findAllByCarBrand(String carBrand);

    List<Cars> findAllByYear(Long year);

}
